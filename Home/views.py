from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Activities
from datetime import datetime
from .forms import Message_Form
response={}

def home(request):
    return render(request, "Home.html")
def abilities(request):
    return render(request, "Abilities.html")
def experience(request):
    return render(request, "Experience.html")
def experience2(request):
    return render(request, "Experience2.html")
def contact(request):
    return render(request, "Contact.html")
def form(request):
    return render(request, "Form.html")

def schedule(request):
    response['form']= Message_Form
    return render(request, 'Schedule.html', response)


def listAct(request):
    message = Activities.objects.all()
    response['message'] = message
    return render (request, "listActivities.html", response)

def add_activity(request):
    form= Message_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        activity = request.POST['activity']
        type_act= request.POST['type_act']
        place= request.POST['place']
        date= request.POST['date']
        dt = datetime.strptime(date, '%Y-%m-%dT%H:%M')
        activities= Activities(activity=activity,type_act=type_act,place=place,date= dt.strftime("%A, %d %B %Y %I:%M%p"))
        activities.save()
        return redirect('Home:listAct')
    else:
        return redirect('Home:schedule',)

def delete(request):
    message = Activities.objects.all().delete()
    response['message'] = message
    return redirect('Home:listAct')
