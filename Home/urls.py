from django.conf.urls import url
from .views import home,abilities,experience2,experience,form,contact,schedule,add_activity,listAct, delete
app_name= 'Home'
urlpatterns = [
 url(r'^$', home, name='home'),
 url(r'^abilities', abilities, name='abilities'),
 url(r'^experience2', experience2, name='experience2'),
 url(r'^experience', experience, name='experience'),
 url(r'^contact', contact, name='contact'),
 url(r'^form', form, name='form'),
 url(r'^schedule', schedule, name='schedule'),
 url(r'^listAct', listAct, name='listAct'),
 url(r'^add_activity', add_activity, name='add_activity'),
  url(r'^delete', delete, name='delete'),
 
 
]

