from django import forms
from .models import Activities
class Message_Form(forms.Form):
    attrs= {Activities:Activities}
    activity = forms.CharField(label='Activities:', required=True, max_length=27,widget=forms.TextInput(attrs=attrs))
    type_act = forms.CharField(label= 'Type of Activities',required=True,max_length=27,widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Place',required=True,max_length=27, widget=forms.TextInput(attrs=attrs))
    date = forms.DateTimeField(label='Date',required=True, widget=forms.DateTimeInput(attrs={'type':'datetime-local'}),input_formats=['%Y-%m-%dT%H:%M'])

